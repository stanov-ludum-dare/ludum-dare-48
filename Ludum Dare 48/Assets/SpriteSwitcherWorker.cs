using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSwitcherWorker : MonoBehaviour
{
    public Sprite[] spritesFocusNone;
    public Sprite[] spritesFocusLow;
    public Sprite[] spritesFocusMiddle;
    public Sprite[] spritesFocusHigh;

    public GameplayConstants constants;

    private Sprite[] _sprites;
    
    IEnumerator Start()
    {
        _sprites = spritesFocusLow;
        var spriteRenderer = GetComponent<SpriteRenderer>();
        int i = 0;
        int previousMoney = -1;
        while (true)
        {
            if (StatsKeeper.Instance.Money > previousMoney)
            {
                i = (i + 1) % _sprites.Length;
                spriteRenderer.sprite = _sprites[i];
                previousMoney = StatsKeeper.Instance.Money;
                yield return new WaitForSeconds(0.3f);
            }
            else
            {
                previousMoney = StatsKeeper.Instance.Money;
                yield return null;
            }
        }
    }

    private void Update()
    {
        if (StatsKeeper.Instance.Focus >= 2 * constants.maxFocus / 3)
        {
            _sprites = spritesFocusHigh;
        }
        else if (StatsKeeper.Instance.Focus >= constants.maxFocus / 3)
        {
            _sprites = spritesFocusMiddle;
        }
        else if (StatsKeeper.Instance.Focus >= 1)
        {
            _sprites = spritesFocusLow;
        }
        else
        {
            _sprites = spritesFocusNone;
        }
    }
}
