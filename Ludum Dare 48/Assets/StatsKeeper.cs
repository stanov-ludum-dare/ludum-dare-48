using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class StatsKeeper : MonoBehaviour
{
    public static StatsKeeper Instance;
    public GameplayConstants constants;
    public int Money { get; private set; }
    public float Focus { get; private set; }

    public string Score
    {
        get
        {
            if (_sceneStarted < 0)
                return "--:--";
            var timeSinceSceneStarted = (int) (Time.time - _sceneStarted);
            var minutes = $"{timeSinceSceneStarted / 60}".PadLeft(2, '0');
            var seconds = $"{timeSinceSceneStarted % 60}".PadLeft(2, '0');
            return $"{minutes}:{seconds}";
        }
    }

    private float _sceneStarted = -1;

    private void Awake()
    {
        Instance = this;
    }

    IEnumerator Start()
    {
        Money = 0;
        Focus = constants.startingFocus;

        while (Instance.Money == 0)
        {
            yield return null;
        }
        
        _sceneStarted = Time.time;
    }

    public static void AddMoney(int add)
    {
        Instance.Money += add;
    }

    public static void IncreaseFocus()
    {
        Instance.Focus += (Instance.constants.maxFocus - Instance.Focus) * Time.fixedDeltaTime * Instance.constants.focusGainSpeed * 0.001f;
    }

    public static void DecreaseFocus(float i)
    {
        Instance.Focus -= i;
    }

    public static bool BuyIfEnoughMoney(int price)
    {
        if (Instance.Money < price)
            return false;

        Instance.Money -= price;
        return true;
    }
}
