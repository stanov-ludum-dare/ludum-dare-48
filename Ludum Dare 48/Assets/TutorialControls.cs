using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class TutorialControls : MonoBehaviour
{
    public GameObject nextTutorial;
    void Update()
    {
        if (StatsKeeper.Instance.Money > 0)
        {
            nextTutorial.SetActive(true);
            Destroy(gameObject);
        }
    }
}
