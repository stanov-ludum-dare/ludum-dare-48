using System.Collections;
using UnityEngine;

public class SpriteSwitcher : MonoBehaviour
{
    public Sprite[] sprites;
    public float rotationSpeed = 1f;

    private float _rotationDirection;
    
    IEnumerator Start()
    {
        _rotationDirection = Mathf.Sign(Random.Range(-1, 1));
        var spriteRenderer = GetComponent<SpriteRenderer>();
        int i = 0;
        while (true)
        {
            spriteRenderer.sprite = sprites[i];
            i = (i + 1) % sprites.Length;
            yield return new WaitForSeconds(0.3f);
        }
    }

    private void Update()
    {
        var angles = transform.eulerAngles;
        angles.z += _rotationDirection * rotationSpeed;
        transform.eulerAngles = angles;
    }
}
