using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class Powerup : MonoBehaviour
{
    [Serializable]
    public enum Type
    {
        Headphones, Room, Lawyer
    }
    
    public new string name;
    public Type type;
    public GameplayConstants constants;
    public KeyCode buyButton = KeyCode.W;
    public int MaxHitPoints
    {
        get { return constants.powerups.First(it => it.type == type).maxHitpoints; }
    }
    public int Price {
        get { return constants.powerups.First(it => it.type == type).price; }
    }
    public int PowerupDamage {
        get { return constants.powerups.First(it => it.type == type).damage; }
    }
    
    public int HitPoints { get; private set; }

    private void Start()
    {
        if (Time.time == 0)
            foreach (Transform i in transform)
            {
                i.gameObject.SetActive(false);
            }
    }

    private void Update()
    {
        if (Input.GetKeyDown(buyButton))
        {
            if (StatsKeeper.BuyIfEnoughMoney(Price))
            {
                foreach (Transform i in transform)
                {
                    i.gameObject.SetActive(true);
                }
                HitPoints = MaxHitPoints;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        other.GetComponent<PowerupTouchable>()?.OnPowerupEnter(PowerupDamage);
        if (--HitPoints <= 0)
            foreach (Transform i in transform)
                i.gameObject.SetActive(false);
    }
}
