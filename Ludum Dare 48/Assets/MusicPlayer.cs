using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public AudioSource mainSource;
    public GameplayConstants constants;

    // Update is called once per frame
    void Update()
    {
        mainSource.volume = 1.0f - StatsKeeper.Instance.Focus / (constants.maxFocus-2);
    }
}
