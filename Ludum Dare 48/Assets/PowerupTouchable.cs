using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PowerupTouchable
{
    void OnPowerupEnter(int damage);
}
