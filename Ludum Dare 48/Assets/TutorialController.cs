using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour
{
    public GameObject controls;
    public GameObject powerups;
    IEnumerator Start()
    {
        controls.SetActive(true);

        while (StatsKeeper.Instance == null || StatsKeeper.Instance.Money < 1000)
        {
            yield return null;
        }
        
        powerups.SetActive(true);
    }
}
