using UnityEngine;

public class Worker : MonoBehaviour, CursorTouchable
{
    public void OnCursorEnter()
    {}

    public void OnCursorTouch()
    {
        StatsKeeper.IncreaseFocus();
        StatsKeeper.AddMoney(Mathf.FloorToInt(StatsKeeper.Instance.Focus));
    }
}
