using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorMover : MonoBehaviour
{
    private Camera _camera;
    private Rigidbody2D _rigidbody;

    void Start()
    {
        _camera = Camera.main;
        Cursor.visible = false;
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        var position = _camera.ScreenToWorldPoint(Input.mousePosition);
        _rigidbody.MovePosition(position);
    }
}
