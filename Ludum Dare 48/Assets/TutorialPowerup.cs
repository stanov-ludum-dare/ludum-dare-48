using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPowerup : MonoBehaviour
{
    public GameObject headphones;
    void Update()
    {
        if (headphones.activeSelf)
        {
            Destroy(gameObject);
        }
    }
}
