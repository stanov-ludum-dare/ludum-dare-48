using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverHandler : MonoBehaviour
{
    public GameOverScreen GameOverScreen;
    public GameObject cursor;
    IEnumerator Start()
    {
        while (StatsKeeper.Instance.Focus > 0)
            yield return null;

        GameOverScreen.show();
        Time.timeScale = 0;
        Cursor.visible = true;
        cursor.SetActive(false);
    }
}
