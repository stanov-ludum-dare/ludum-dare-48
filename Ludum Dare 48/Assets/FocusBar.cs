using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class FocusBar : MonoBehaviour
{
    [FormerlySerializedAs("bar")] public RectTransform barTransform;
    public Image barImage;
    public GameplayConstants constants;

    private float _maxHeight;
    void Start()
    {
        _maxHeight = barTransform.rect.height;
    }

    void Update()
    {
        barTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _maxHeight * StatsKeeper.Instance.Focus / constants.maxFocus);

        if (StatsKeeper.Instance.Focus < 1)
            barImage.color = Color.red;
        else
        {
            barImage.color = Color.green;
        }
    }
}
