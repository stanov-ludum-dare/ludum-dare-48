using UnityEngine;

public class ThoughtHealth : MonoBehaviour, CursorTouchable, PowerupTouchable
{
    public GameplayConstants constants;

    public float hitpoints;

    public void OnCursorEnter()
    {
        StatsKeeper.DecreaseFocus(constants.focusDecreasePerHit);
    }

    public void OnCursorTouch()
    {
        hitpoints -= Time.fixedDeltaTime / constants.secondsToDamageOnePoint;
        
        if (hitpoints <= 0)
            Destroy(gameObject);
    }

    public void OnPowerupEnter(int damage)
    {
        hitpoints -= damage;
        
        if (hitpoints <= 0)
            Destroy(gameObject);
    }
}
