using System;
using UnityEngine;
using UnityEngine.UI;

public class FocusShower : MonoBehaviour
{
    public Text text;
    private string template;
    void Start()
    {
        template = text.text;
    }

    void Update()
    {
        float focus = StatsKeeper.Instance.Focus;
        focus = Mathf.Max(0, focus);
        focus = Mathf.FloorToInt(focus);
        text.text = template.Replace("XXX", focus.ToString());
    }
}
