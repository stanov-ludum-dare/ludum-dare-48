using UnityEngine;

public class ThoughtMover : MonoBehaviour
{
    public GameplayConstants constants;
    private Rigidbody2D _rigidbody;
    private float _bounceVelocity;
    private float _speedMultiplier;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _speedMultiplier = Random.Range(0.9f, 1.1f);
    }

    void FixedUpdate()
    {
        if (_bounceVelocity > 0)
        {
            _rigidbody.MovePosition(_rigidbody.position + (Vector2)(transform.position - Vector3.zero).normalized * (Time.deltaTime * _bounceVelocity));
            _bounceVelocity -= 10 * Time.deltaTime;
            return;
        }
        
        _rigidbody.MovePosition(_rigidbody.position + (Vector2)(Vector3.zero - transform.position).normalized * (Time.deltaTime * _speedMultiplier));
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Worker>() != null)
        {
            _bounceVelocity = constants.bounceVelocity;
            StatsKeeper.DecreaseFocus(constants.focusDecreasePerMiss);
        }
    }
}
