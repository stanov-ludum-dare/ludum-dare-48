using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDistractions : MonoBehaviour
{
    private float _previousFocus = -1;
    void Update()
    {
        var current = StatsKeeper.Instance.Focus;
        
        if (_previousFocus - current > 0.4f )
            Destroy(gameObject);

        _previousFocus = current;
    }
}
