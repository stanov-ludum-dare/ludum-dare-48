using System;
using UnityEngine;

public class GameplayConstants : ScriptableObject
{
    public int startingFocus = 1;
    public int maxFocus = 10;
    public int focusGainSpeed = 100;
    public float focusDecreasePerHit = 0.5f;
    public float focusDecreasePerMiss = 1f;
    public float bounceVelocity = 10f;
    public float thoughtGenerationPeriod = 1f;
    public float threatPerSecond = 1f;
    public float threatPerSecondMultiplier = 1.01f;
    public float secondsToDamageOnePoint = 0.5f;
    public PowerupConstants[] powerups;
    public ThoughtConstants[] thoughts;
}

[Serializable]
public class PowerupConstants
{
    public Powerup.Type type;
    public int maxHitpoints;
    public int price;
    public int damage;
}

[Serializable]
public class ThoughtConstants
{
    public GameObject prefab;
    public int threat;
    public int hitpoints;
}