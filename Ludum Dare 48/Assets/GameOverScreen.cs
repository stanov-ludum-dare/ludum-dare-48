using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    public Text text;
    void Awake()
    {
        if (Time.time == 0)
            gameObject.SetActive(false);
    }

    public void show()
    {
        text.text = text.text.Replace("%SCORE%", StatsKeeper.Instance.Score);
        gameObject.SetActive(true);
    }
}
