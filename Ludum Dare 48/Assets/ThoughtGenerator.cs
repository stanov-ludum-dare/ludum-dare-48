using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ThoughtGenerator : MonoBehaviour
{
    public GameplayConstants constants;

    private List<Transform> _generators;
    public float Threat { get; private set; }
    private float _threatPerSecond;
    
    IEnumerator Start()
    {
        _generators = GetComponentsInChildren<Transform>().ToList();
        _generators.Remove(transform);
        _threatPerSecond = constants.threatPerSecond;
        
        float waitFor = constants.thoughtGenerationPeriod;
        
        while (true)
        {
            var generator = _generators[Random.Range(0, _generators.Count)];
            var thought = constants.thoughts.LastOrDefault(it => it.threat < Threat);
            if (thought != null)
            {
                var gameObject = Instantiate(thought.prefab, generator.position, Quaternion.identity);
                gameObject.GetComponent<ThoughtHealth>().hitpoints = thought.hitpoints - 0.99f;
                Threat -= thought.threat;
            }
            
            _threatPerSecond *= constants.threatPerSecondMultiplier;
            yield return new WaitForSeconds(waitFor);
        }
    }

    private void FixedUpdate()
    {
        if (StatsKeeper.Instance.Score == "--:--")
            return;
        
        Threat += _threatPerSecond * Time.fixedDeltaTime;
    }
}
