using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThreatDebug : MonoBehaviour
{
    public ThoughtGenerator generator;
    private Text text;
    private string template;

    private void Awake()
    {
        text = GetComponent<Text>();
        template = text.text;
    }

    void Update()
    {
        text.text = template.Replace("XXX", generator.Threat.ToString());
    }
}
