using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface CursorTouchable
{
    void OnCursorEnter();
    void OnCursorTouch();
}
