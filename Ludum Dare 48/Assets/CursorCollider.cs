using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CursorCollider : MonoBehaviour
{
    private readonly List<CursorTouchable> _currentlyColliding = new List<CursorTouchable>();

    private void OnTriggerEnter2D(Collider2D other)
    {
        var touchable = other.gameObject.GetComponent<CursorTouchable>();
        if (touchable != null)
        {
            _currentlyColliding.Add(touchable);
            touchable.OnCursorEnter();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var touchables = other.gameObject.GetComponents<CursorTouchable>();
        _currentlyColliding.RemoveAll(touchable => touchables.Contains(touchable));
    }

    private void FixedUpdate()
    {
        foreach (var touchable in new List<CursorTouchable>(_currentlyColliding))
        {
            touchable.OnCursorTouch();
        }
    }
}
