using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light : MonoBehaviour
{
    public GameplayConstants constants;
    private UnityEngine.Light _light;
    void Awake()
    {
        _light = GetComponent<UnityEngine.Light>();
    }

    void Update()
    {
        _light.spotAngle = 110 - (110 - 10) * (StatsKeeper.Instance.Focus / constants.maxFocus); 
    }
}
