using UnityEngine;
using UnityEngine.UI;

public class PowerupDisplayer : MonoBehaviour
{
    public Powerup powerup;
    public Text text;
    public RectTransform remaining;

    private float _maxHeight;

    void Start()
    {
        text.text = $"{powerup.name}\n${powerup.Price}\n\n{powerup.buyButton}";
        _maxHeight = remaining.rect.height;
        SetHeightOfRemaining(0);
    }

    private void Update()
    {
        SetHeightOfRemaining(_maxHeight * (powerup.HitPoints / (float) powerup.MaxHitPoints));
    }

    private void SetHeightOfRemaining(float height)
    {
        remaining.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }
}
