using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyShower : MonoBehaviour
{
    public Text text;
    private string template;
    void Start()
    {
        template = text.text;
    }

    void Update()
    {
        text.text = template.Replace("XXX", StatsKeeper.Instance.Money.ToString());
    }
}
